# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.2.0](https://github.com/DataShades/ckanext-comments/compare/v0.1.2...v0.2.0) (2023-04-03)


### ⚠ BREAKING CHANGES

* replace const module with config

### Features

* replace const module with config ([6fe009b](https://github.com/DataShades/ckanext-comments/commit/6fe009be3510bcaa06d6bb5b14fc888896a5d517))


### Bug Fixes

* resolve circular import of Thread from Comment ([9054878](https://github.com/DataShades/ckanext-comments/commit/905487830017cc13bcbab4d27ce448bcc705c16c))

### [0.1.2](https://github.com/DataShades/ckanext-comments/compare/v0.1.0...v0.1.2) (2023-03-03)


### Bug Fixes

* add config declaration to manifest ([247df40](https://github.com/DataShades/ckanext-comments/commit/247df40982a2319b58cae902624c3f576fb87042))

### [0.1.1](https://github.com/DataShades/ckanext-comments/compare/v0.1.0...v0.1.1) (2023-03-01)

## [0.1.0](https://github.com/DataShades/ckanext-comments/compare/v0.0.19...v0.1.0) (2022-11-09)


### Bug Fixes

* allow editing own draft comments by default ([fe83008](https://github.com/DataShades/ckanext-comments/commit/fe830083d80a0d494cb021ef42f9432b86dc811a))
