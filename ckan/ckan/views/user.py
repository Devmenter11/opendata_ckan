# encoding: utf-8
import json
import logging

from flask import Blueprint
from flask.views import MethodView
from ckan.common import asbool
from six import text_type, ensure_str
import dominate.tags as dom_tags

# import openpyxl
import ckan.lib.authenticator as authenticator
import ckan.lib.base as base
import ckan.lib.captcha as captcha
import ckan.lib.helpers as h
import ckan.lib.mailer as mailer
import ckan.lib.navl.dictization_functions as dictization_functions
import ckan.logic as logic
import ckan.logic.schema as schema
import ckan.model as model
import ckan.plugins as plugins
from ckan import authz
from ckan.common import _, config, g, request
from ckan.lib.helpers import get_organs_list
from datetime import datetime

log = logging.getLogger(__name__)

# hooks for subclasses
new_user_form = "user/new_user_form.html"
# quanlhb 21/02
new_user_form_organ = "user/new_user_form_organ.html"
# quanlhb 17/04
new_user_form_add = "user/new_user_form_add.html"
# quanlhb 29/06
edit_user_form_acc = "user/edit_user_form_acc.html"
edit_user_form_contact = "user/edit_user_form_contact.html"

edit_user_form = "user/edit_user_form.html"

user = Blueprint("user", __name__, url_prefix="/user")


def _get_repoze_handler(handler_name):
    """Returns the URL that repoze.who will respond to and perform a
    login or logout."""
    return getattr(request.environ["repoze.who.plugins"]["friendlyform"], handler_name)


def set_repoze_user(user_id, resp):
    """Set the repoze.who cookie to match a given user_id"""
    if "repoze.who.plugins" in request.environ:
        rememberer = request.environ["repoze.who.plugins"]["friendlyform"]
        identity = {"repoze.who.userid": user_id}
        resp.headers.extend(rememberer.remember(request.environ, identity))


def _edit_form_to_db_schema():
    return schema.user_edit_form_schema()


def _new_form_to_db_schema():
    return schema.user_new_form_schema()


def _extra_template_variables(context, data_dict):
    is_sysadmin = authz.is_sysadmin(g.user)
    try:
        user_dict = logic.get_action("user_show")(context, data_dict)
    except logic.NotFound:
        base.abort(404, _("User not found"))
    except logic.NotAuthorized:
        base.abort(403, _("Not authorized to see this page"))

    is_myself = user_dict["name"] == g.user
    about_formatted = h.render_markdown(user_dict["about"])
    extra = {
        "is_sysadmin": is_sysadmin,
        "user_dict": user_dict,
        "is_myself": is_myself,
        "about_formatted": about_formatted,
    }
    return extra


@user.before_request
def before_request():
    try:
        context = dict(model=model, user=g.user, auth_user_obj=g.userobj)
        logic.check_access("site_read", context)
    except logic.NotAuthorized:
        blueprint, action = plugins.toolkit.get_endpoint()
        if action not in (
            "login",
            "request_reset",
            "perform_reset",
        ):
            base.abort(403, _("Not authorized to see this page"))


# def index():
#     page_number = h.get_page_number(request.params)
#     q = request.params.get(u'q', u'')
#     order_by = request.params.get(u'order_by', u'name')
#     limit = int(
#         request.params.get(u'limit', config.get(u'ckan.user_list_limit',10)))
#     context = {
#         u'return_query': True,
#         u'user': g.user,
#         u'auth_user_obj': g.userobj
#     }

#     data_dict = {
#         u'q': q,
#         u'order_by': order_by
#     }

#     try:
#         logic.check_access(u'user_list', context, data_dict)
#     except logic.NotAuthorized:
#         base.abort(403, _(u'Not authorized to see this page'))

#     users_list = logic.get_action(u'user_list')(context, data_dict)

#     page = h.Page(
#         collection=users_list,
#         page=page_number,
#         url=h.pager_url,
#         item_count=users_list.count(),
#         items_per_page=limit)


#     extra_vars = {u'page': page, u'q': q, u'order_by': order_by}
#     return base.render(u'user/list.html', extra_vars)


class Index(MethodView):
    def _edit_form_to_db_schema():
        return schema.user_edit_form_schema()

    def _prepare(self, id):
        context = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        if id is None:
            if g.userobj:
                id = g.userobj.id
            else:
                base.abort(400, _("No user specified"))
        data_dict = {"id": id}

        try:
            logic.check_access("user_update", context, data_dict)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit a user."))
        return context, id

    def post(self):
        """Delete user with id passed as parameter"""
        users_delete = request.form.getlist("check_user_d")
        users_reset = request.form.getlist("check_user_r")

        context_reset = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }

        for user in users_reset:
            user_dict = logic.get_action("user_show")(context_reset, {"id": user})
            user_dict["id"] = user
            user_dict["password1"] = "123abc@A"
            user_dict["password2"] = "123abc@A"
            logic.get_action("user_update")(context_reset, user_dict)
            h.flash_notice(_("Mật khẩu người dùng đã được đặt lại."))

        context = {
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        for userd in users_delete:
            logic.get_action("user_delete")(context, {"id": userd})
            if g.userobj.id == userd:
                return logout()

        # # Đường dẫn đến file xlsx
        # file_path = request.form['myFile']

        # # Mở file
        # workbook = openpyxl.load_workbook(file_path)

        # # Chọn sheet cần đọc
        # selected_sheet = workbook.active

        # # Lấy dữ liệu từ sheet và chuyển thành danh sách các dict
        # sheet_data = []
        # for row in selected_sheet.iter_rows(values_only=True):
        #     data_row = {selected_sheet.cell(1, col_index).value: cell_value for col_index, cell_value in enumerate(row, start=1)}
        #     sheet_data.append(data_row)

        # h.flash_success(u'Thêm mới người dùng %s thành công' % sheet_data)

        # # In ra dữ liệu dưới dạng danh sách các dict
        # # print(sheet_data)

        user_index = h.url_for("user.index")
        return h.redirect_to(user_index)

    def get(self):
        page_number = h.get_page_number(request.params)
        q = request.params.get("q", "")
        # quanlhb 11/01
        order_by = request.params.get("order_by", "sysadmin")
        limit = int(request.params.get("limit", config.get("ckan.user_list_limit", 10)))
        context = {"return_query": True, "user": g.user, "auth_user_obj": g.userobj}

        data_dict = {"q": q, "order_by": order_by}

        if authz.is_sysadmin(g.user):
            logic.check_access("user_list", context, data_dict)
        else:
            base.abort(403, _("Need to be system administrator to administer"))

        users_list = logic.get_action("user_list")(context, data_dict)
        users_pending = logic.get_action("user_list_pending")(context, data_dict)
        count_pending = users_pending.count()
        page = h.Page(
            collection=users_list,
            page=page_number,
            url=h.pager_url,
            item_count=users_list.count(),
            items_per_page=limit,
        )

        extra_vars = {
            "page": page,
            "q": q,
            "order_by": order_by,
            "count_pending": count_pending,
        }

        return base.render("user/list.html", extra_vars)


# quanlhb 02/10
class Index_organ(MethodView):
    def _edit_form_to_db_schema():
        return schema.user_edit_form_schema()

    def _prepare(self, id):
        context = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        if id is None:
            if g.userobj:
                id = g.userobj.id
            else:
                base.abort(400, _("No user specified"))
        data_dict = {"id": id}

        try:
            logic.check_access("user_update", context, data_dict)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit a user."))
        return context, id

    def post(self):
        """Delete user with id passed as parameter"""
        users_delete = request.form.getlist("check_user_d")
        users_reset = request.form.getlist("check_user_r")

        context_reset = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }

        for user in users_reset:
            user_dict = logic.get_action("user_show")(context_reset, {"id": user})
            user_dict["id"] = user
            user_dict["password1"] = "123abc@A"
            user_dict["password2"] = "123abc@A"
            logic.get_action("user_update")(context_reset, user_dict)
            h.flash_notice(_("Mật khẩu người dùng đã được đặt lại."))

        context = {
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        for userd in users_delete:
            logic.get_action("user_delete")(context, {"id": userd})
            if g.userobj.id == userd:
                return logout()

        # # Đường dẫn đến file xlsx
        # file_path = request.form['myFile']

        # # Mở file
        # workbook = openpyxl.load_workbook(file_path)

        # # Chọn sheet cần đọc
        # selected_sheet = workbook.active

        # # Lấy dữ liệu từ sheet và chuyển thành danh sách các dict
        # sheet_data = []
        # for row in selected_sheet.iter_rows(values_only=True):
        #     data_row = {selected_sheet.cell(1, col_index).value: cell_value for col_index, cell_value in enumerate(row, start=1)}
        #     sheet_data.append(data_row)

        # h.flash_success(u'Thêm mới người dùng %s thành công' % sheet_data)

        # # In ra dữ liệu dưới dạng danh sách các dict
        # # print(sheet_data)

        user_index = h.url_for("user.index_organ")
        return h.redirect_to(user_index)

    def get(self):
        page_number = h.get_page_number(request.params)
        q = request.params.get("q", "")
        order_by = request.params.get("order_by", "name")
        limit = int(request.params.get("limit", config.get("ckan.user_list_limit", 10)))
        context = {"return_query": True, "user": g.user, "auth_user_obj": g.userobj}

        data_dict = {"q": q, "order_by": order_by}

        if authz.is_sysadmin(g.user):
            logic.check_access("user_list_organ", context, data_dict)
        else:
            base.abort(403, _("Need to be system administrator to administer"))

        users_list_organ = logic.get_action("user_list_organ")(context, data_dict)
        users_pending = logic.get_action("user_list_pending")(context, data_dict)
        count_pending = users_pending.count()

        page = h.Page(
            collection=users_list_organ,
            page=page_number,
            url=h.pager_url,
            item_count=users_list_organ.count(),
            items_per_page=limit,
        )

        extra_vars = {
            "page": page,
            "q": q,
            "order_by": order_by,
            "count_pending": count_pending,
        }
        return base.render("user/list_organ.html", extra_vars)


def me():
    return h.redirect_to(config.get("ckan.route_after_login", "home.index"))


def read(id):
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
        "for_view": True,
    }
    data_dict = {
        "id": id,
        "user_obj": g.userobj,
        "include_datasets": True,
        "include_num_followers": True,
    }
    # FIXME: line 331 in multilingual plugins expects facets to be defined.
    # any ideas?
    g.fields = []

    # quanlhb 06/09
    current_user = context["user"]
    current_user1 = str(data_dict["id"])
    if authz.is_sysadmin(g.user):
        pass
    elif current_user != current_user1:
        return base.abort(403, _("Not authorized to see this page"))

    extra_vars = _extra_template_variables(context, data_dict)
    if extra_vars is None:
        return h.redirect_to("user.login")
    return base.render("user/read.html", extra_vars)


# quanlhb 04/07
def read_user(id):
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
        "for_view": True,
    }
    data_dict = {
        "id": id,
        "user_obj": g.userobj,
        "include_datasets": True,
        "include_num_followers": True,
    }
    # FIXME: line 331 in multilingual plugins expects facets to be defined.
    # any ideas?
    g.fields = []

    # quanlhb 06/09
    current_user = context["user"]
    current_user1 = str(data_dict["id"])
    if authz.is_sysadmin(g.user):
        pass
    elif current_user != current_user1:
        return base.abort(403, _("Not authorized to see this page"))

    extra_vars = _extra_template_variables(context, data_dict)
    if extra_vars is None:
        return h.redirect_to("user.login")
    return base.render("user/read_user.html", extra_vars)


# giangpd15/06
class ListPending(MethodView):
    def _edit_form_to_db_schema():
        return schema.user_edit_form_schema()

    def _prepare(self, id):
        context = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        if id is None:
            if g.userobj:
                id = g.userobj.id
            else:
                base.abort(400, _("No user specified"))
        data_dict = {"id": id}

        try:
            logic.check_access("user_update", context, data_dict)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit a user."))
        return context, id

    def post(self, id=None):
        users_a = request.form.getlist("check_user_a")
        users_d = request.form.getlist("check_user_re")
        context = {
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }

        context1, id = self._prepare(id)
        data_dict = {"id": id, "include_plugin_extras": True}
        all_role = logic.get_action("get_all_roles")(context1, data_dict)
        data_dict_patch = dict(all_role)

        for user_a in users_a:
            logic.get_action("user_patch")(
                context, {"id": user_a, "state": model.State.ACTIVE}
            )

        for user_a in users_a:
            logic.get_action("user_patch")(
                context1, {"id": user_a, "plugin_extras": data_dict_patch}
            )

        for user_d in users_d:
            logic.get_action("user_patch")(
                context, {"id": user_d, "state": model.State.DELETED}
            )

        return h.redirect_to("user.index_pending")

    def get(self):
        page_number = h.get_page_number(request.params)
        q = request.params.get("q", "")
        order_by = request.params.get("order_by", "name")
        limit = int(request.params.get("limit", config.get("ckan.user_list_limit", 10)))
        context = {"return_query": True, "user": g.user, "auth_user_obj": g.userobj}

        data_dict = {"q": q, "order_by": order_by}

        if authz.is_sysadmin(g.user):
            logic.check_access("user_list_pending", context, data_dict)
        else:
            base.abort(403, _("Need to be system administrator to administer"))

        users_pending = logic.get_action("user_list_pending")(context, data_dict)
        count_pending = users_pending.count()

        page = h.Page(
            collection=users_pending,
            page=page_number,
            url=h.pager_url,
            item_count=users_pending.count(),
            items_per_page=limit,
        )

        extra_vars = {
            "page": page,
            "q": q,
            "order_by": order_by,
            "count_pending": count_pending,
        }
        return base.render("user/list_pending.html", extra_vars)


# giangptd08/06
class Permissions(MethodView):
    def _edit_form_to_db_schema():
        return schema.user_edit_form_schema()

    def _prepare(self, id):
        context = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        if id is None:
            if g.userobj:
                id = g.userobj.id
            else:
                base.abort(400, _("No user specified"))
        data_dict = {"id": id}

        try:
            logic.check_access("user_update", context, data_dict)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit a user."))
        return context, id

    def post(self, id=None):
        q = request.params.get("q", "")
        order_by = request.params.get("order_by", "name")
        context_list_user = {
            "return_query": True,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        data_dict_list_user = {"q": q, "order_by": order_by}
        users_list = logic.get_action("user_list_permission")(
            context_list_user, data_dict_list_user
        )
        page = h.Page(collection=users_list, item_count=users_list.count())
        total_user = page.items

        role = request.form["roles"]
        permision = request.form.getlist("permissions")
        act = request.form["activate"]
        data = dict(
            [
                (role, [permision, str(act)]),
            ]
        )

        context, id = self._prepare(id)
        data_dict = {"id": id, "include_plugin_extras": True}
        all_role = logic.get_action("get_all_roles")(context, data_dict)
        data_dict_patch = dict(all_role)
        data_dict_patch.update(data)

        # history
        current_datetime = datetime.now()
        # real_time = dict([('admin created roles' , [str(current_datetime)])])
        real_time = dict([(str(current_datetime), ["Tạo mới nhóm quyền", str(role)])])
        # history = dict()
        historys = logic.get_action("get_history_roles")(context, data_dict)
        history = dict(historys)
        history.update(real_time)

        data_patch = {"roles": dict(data_dict_patch), "history": dict(history)}

        for user in total_user:
            logic.get_action("user_patch")(
                context, {"id": user[0].id, "plugin_extras": data_patch}
            )

        return h.redirect_to("user.permissions")

    def get(self, id=None):
        """display about page"""

        context1, id = self._prepare(id)
        data_dict = {"id": id, "include_plugin_extras": True}
        # quanlhb 11/01 fix permissions
        all_role = logic.get_action("get_all_roles")(context1, data_dict)
        roles = [(key, all_role[key]) for key in all_role]
        # old_role = authz.ROLE_PERMISSION
        # roles = [(key, old_role[key]) for key in old_role]
        pers = []
        for role in all_role:
            pers.append(role)

        per = json.dumps(pers)

        all_history = logic.get_action("get_history_roles")(context1, data_dict)
        historys = [(key, all_history[key]) for key in all_history]

        organ_lists = []
        members = []
        users = []
        ors = []
        context = {"model": model, "session": model.Session, "user": g.user}
        organs = get_organs_list()
        for org in organs:
            ors.append(org["display_name"])

        orgn = json.dumps(ors)

        for organ in organs:
            t = ()
            ol = list(t)
            ol.insert(0, organ["id"])
            ol.insert(1, organ["display_name"])
            organ_lists.append(tuple(ol))

        for organ_list, organ_name in organ_lists:
            sub_members = []
            subs_members = []
            sub_user = []
            sub_members = logic.get_action("member_list")(
                context, {"id": organ_list, "object_type": "user"}
            )

            for m in sub_members:
                l = list(m)
                l.insert(3, organ_list)
                l.insert(4, organ_name)
                subs_members.append(tuple(l))
                lu = m[0]
                sub_user.append(lu)

            users.extend(sub_user)
            members.extend(subs_members)

        extra_vars = {
            "roles": roles,
            "members": members,
            "historys": historys,
            "per": per,
            "pers": pers,
            "orgn": orgn,
        }
        return base.render("user/permissions.html", extra_vars)


def _prepare(id):
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }
    if id is None:
        if g.userobj:
            id = g.userobj.id
    return context, id


def delete_permission(id=None):
    extra_vars = {}

    context = {
        "save": "save" in request.form,
        "schema": _edit_form_to_db_schema(),
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }
    user_get = g.userobj.id
    all_role = logic.get_action("get_all_roles")(context, {"id": user_get})
    roles_covert = dict(all_role)

    q = request.params.get("q", "")
    order_by = request.params.get("order_by", "name")
    context_list_user = {
        "return_query": True,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }

    data_dict_list_user = {"q": q, "order_by": order_by}
    users_list = logic.get_action("user_list_permission")(
        context_list_user, data_dict_list_user
    )
    page = h.Page(collection=users_list, item_count=users_list.count())
    total_user = page.items

    context1, id = _prepare(id)

    if request.method == "POST":
        role = request.params.get("role")
        roles_covert.pop(str(role))

        # history
        current_datetime = datetime.now()
        # real_time = dict([('admin delete roles' , [str(current_datetime)])])
        real_time = dict([(str(current_datetime), ["Xóa nhóm quyền", str(role)])])
        # history = dict()
        historys = logic.get_action("get_history_roles")(context, {"id": user_get})
        history = dict(historys)
        history.update(real_time)

        data_patch = {"roles": dict(roles_covert), "history": dict(history)}

        for user in total_user:
            logic.get_action("user_patch")(
                context1, {"id": user[0].id, "plugin_extras": data_patch}
            )
        return h.redirect_to("user.permissions")
    extra_vars = {"role": role}
    return base.render("user/confirm_delete_permission.html", extra_vars)


class EditPermission(MethodView):
    def _edit_form_to_db_schema():
        return schema.user_edit_form_schema()

    def _prepare(self, id):
        context = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        user_get = g.userobj.id
        all_role = logic.get_action("get_all_roles")(context, {"id": user_get})
        list_role = {key: value[0] for key, value in all_role.items()}
        roles_covert = dict(list_role)
        data = roles_covert[str(id)]

        l = dict(all_role)
        per_act = l[str(id)]
        activate = per_act[1]

        return data, id, activate

    def post(self, id=None):
        q = request.params.get("q", "")
        order_by = request.params.get("order_by", "name")
        context_list_user = {
            "return_query": True,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        data_dict_list_user = {"q": q, "order_by": order_by}
        users_list = logic.get_action("user_list_permission")(
            context_list_user, data_dict_list_user
        )
        page = h.Page(collection=users_list, item_count=users_list.count())
        total_user = page.items

        role = request.form["roles"]
        permision = request.form.getlist("permissions")
        act = request.form["activate"]
        data = dict(
            [
                (role, [permision, str(act)]),
            ]
        )

        context = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        user_get = g.userobj.id
        all_role = logic.get_action("get_all_roles")(context, {"id": user_get})
        data_dict_patch = dict(all_role)
        data_dict_patch.pop(str(id))
        data_dict_patch.update(data)

        # history
        current_datetime = datetime.now()
        # real_time = dict([('admin created roles' , [str(current_datetime)])])
        real_time = dict([(str(current_datetime), ["Cập nhật nhóm quyền", str(role)])])
        # history = dict()
        historys = logic.get_action("get_history_roles")(context, {"id": user_get})
        history = dict(historys)
        history.update(real_time)

        data_patch = {"roles": dict(data_dict_patch), "history": dict(history)}

        for user in total_user:
            logic.get_action("user_patch")(
                context, {"id": user[0].id, "plugin_extras": data_patch}
            )

        return h.redirect_to("user.permissions")

    def get(self, id=None, data=None):
        data, id, activate = self._prepare(id)
        permissions = json.dumps(data)
        activates = json.dumps(activate)
        extra_vars = {
            "permissions": permissions,
            "data": data,
            "id": id,
            "activates": activates,
        }
        return base.render("user/edit_permission.html", extra_vars)


# giangptd 12/06
def accept_pending(id):
    """Accept user with id passed as parameter"""
    extra_vars = {}

    context = {
        "save": "save" in request.form,
        "schema": _edit_form_to_db_schema(),
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }

    data_old = logic.get_action("user_show")(context, {"id": id})
    about_old = data_old["about"]
    arr = h.get_phone_pass(about_old)

    ad = g.userobj.id
    data_dict = {"id": ad, "include_plugin_extras": True}
    all_role = logic.get_action("get_roles_pending")(context, data_dict)
    data_dict_patch = dict(all_role)

    data_old["id"] = id
    data_old["about"] = (
        str(arr[0]),
        str(arr[1]),
        str(arr[2]),
        None,
        str(arr[10]),
        "member",
    )
    data_old["state"] = model.State.ACTIVE
    data_old["plugin_extras"] = data_dict_patch

    id_organ = str(arr[10])

    data_dict_organ = {"id": id_organ, "username": id, "role": "member"}

    if request.method == "POST":
        logic.get_action("user_update")(context, data_old)
        if id_organ != None:
            logic.get_action("group_member_create")(context, data_dict_organ)
        return h.redirect_to("user.index_pending")
    extra_vars = {"id": id}
    return base.render("user/confirm_accept_user.html", extra_vars)


# quanlhb 14/06
def unlock_user(id):
    """Accept user with id passed as parameter"""
    extra_vars = {}
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }
    if request.method == "POST":
        logic.get_action("user_patch")(context, {"id": id, "state": model.State.ACTIVE})
        return h.redirect_to("user.index")
    extra_vars = {"id": id}
    return base.render("user/confirm_accept_user.html", extra_vars)


def lock_user(id):
    """Accept user with id passed as parameter"""
    extra_vars = {}
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }

    if request.method == "POST":
        logic.get_action("user_patch")(context, {"id": id, "state": model.State.LOCKED})
        return h.redirect_to("user.index")
    extra_vars = {"id": id}
    return base.render("user/confirm_lock_user.html", extra_vars)


def delete_request_user(id):
    """Accept user with id passed as parameter"""
    extra_vars = {}
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }

    if request.method == "POST":
        logic.get_action("user_patch")(
            context, {"id": id, "state": model.State.DELETED}
        )
        return h.redirect_to("user.index_pending")
    extra_vars = {"id": id}
    return base.render("user/confirm_delete_request_user.html", extra_vars)


def reset_password(id):
    """Accept user with id passed as parameter"""
    extra_vars = {}
    context = {
        "save": "save" in request.form,
        "schema": _edit_form_to_db_schema(),
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }

    user_dict = logic.get_action("user_show")(context, {"id": id})
    user_dict["id"] = id
    user_dict["password1"] = "123abc@A"
    user_dict["password2"] = "123abc@A"

    if request.method == "POST":
        logic.get_action("user_update")(context, user_dict)
        h.flash_notice(_("Mật khẩu người dùng đã được đặt lại."))
        return h.redirect_to("user.index")
    extra_vars = {"id": id}
    return base.render("user/confirm_reset_password.html", extra_vars)


class ApiTokenView(MethodView):
    def get(self, id, data=None, errors=None, error_summary=None):
        context = {
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
            "for_view": True,
            "include_plugin_extras": True,
        }
        try:
            tokens = logic.get_action("api_token_list")(context, {"user": id})
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to view API tokens."))

        data_dict = {
            "id": id,
            "user_obj": g.userobj,
            "include_datasets": True,
            "include_num_followers": True,
        }

        extra_vars = _extra_template_variables(context, data_dict)
        if extra_vars is None:
            return h.redirect_to("user.login")
        extra_vars["tokens"] = tokens
        extra_vars.update(
            {"data": data, "errors": errors, "error_summary": error_summary}
        )
        return base.render("user/api_tokens.html", extra_vars)

    def post(self, id):
        context = {"model": model}

        data_dict = logic.clean_dict(
            dictization_functions.unflatten(
                logic.tuplize_dict(logic.parse_params(request.form))
            )
        )

        data_dict["user"] = id
        try:
            token = logic.get_action("api_token_create")(context, data_dict)["token"]
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to create API tokens."))
        except logic.ValidationError as e:
            errors = e.error_dict
            error_summary = e.error_summary
            return self.get(id, data_dict, errors, error_summary)

        copy_btn = dom_tags.button(
            dom_tags.i("", {"class": "fa fa-copy"}),
            {
                "type": "button",
                "class": "btn btn-default btn-xs",
                "data-module": "copy-into-buffer",
                "data-module-copy-value": ensure_str(token),
            },
        )
        h.flash_success(
            _(
                'API Token created: <code style="word-break:break-all;">'
                "{token}</code> {copy}<br>"
                "Make sure to copy it now, "
                "you won't be able to see it again!"
            ).format(token=ensure_str(token), copy=copy_btn),
            True,
        )
        return h.redirect_to("user.api_tokens", id=id)


def api_token_revoke(id, jti):
    context = {"model": model}
    try:
        logic.get_action("api_token_revoke")(context, {"jti": jti})
    except logic.NotAuthorized:
        base.abort(403, _("Unauthorized to revoke API tokens."))
    return h.redirect_to("user.api_tokens", id=id)


class EditView(MethodView):
    def _prepare(self, id):
        context = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        if id is None:
            if g.userobj:
                id = g.userobj.id
            else:
                base.abort(400, _("No user specified"))
        data_dict = {"id": id}

        try:
            logic.check_access("user_update", context, data_dict)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit a user."))
        return context, id

    def post(self, id=None):
        context, id = self._prepare(id)
        if not context["save"]:
            return self.get(id)

        if id in (g.userobj.id, g.userobj.name):
            current_user = True
        else:
            current_user = False
        old_username = g.userobj.name

        try:
            data_dict = logic.clean_dict(
                dictization_functions.unflatten(
                    logic.tuplize_dict(logic.parse_params(request.form))
                )
            )
            data_dict.update(
                logic.clean_dict(
                    dictization_functions.unflatten(
                        logic.tuplize_dict(logic.parse_params(request.files))
                    )
                )
            )

        except dictization_functions.DataError:
            base.abort(400, _("Integrity Error"))
        data_dict.setdefault("activity_streams_email_notifications", False)

        context["message"] = data_dict.get("log_message", "")
        data_dict["id"] = id
        data_dict["about"] = (
            str(data_dict["phone"]),
            str(data_dict["passport"]),
            str(data_dict["abouts"]),
        )
        email_changed = data_dict["email"] != g.userobj.email

        if (data_dict["password1"] and data_dict["password2"]) or email_changed:
            identity = {"login": g.user, "password": data_dict["old_password"]}
            auth = authenticator.UsernamePasswordAuthenticator()

            auth_user_id = auth.authenticate(request.environ, identity)
            if auth_user_id:
                auth_user_id = auth_user_id.split(",")[0]
            if auth_user_id != g.userobj.id:
                errors = {"oldpassword": [_("Mật khẩu không chính xác")]}
                error_summary = (
                    {_("Old Password"): _("Mật khẩu không chính xác")}
                    if not g.userobj.sysadmin
                    else {_("Sysadmin Password"): _("Mật khẩu không chính xác")}
                )
                return self.get(id, data_dict, errors, error_summary)

        try:
            user = logic.get_action("user_update")(context, data_dict)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit user %s") % id)
        except logic.NotFound:
            base.abort(404, _("User not found"))
        except logic.ValidationError as e:
            errors = e.error_dict
            error_summary = e.error_summary
            return self.get(id, data_dict, errors, error_summary)

        h.flash_success(_("Profile updated"))
        resp = h.redirect_to("user.read", id=user["name"])
        if current_user and data_dict["name"] != old_username:
            # Changing currently logged in user's name.
            # Update repoze.who cookie to match
            set_repoze_user(data_dict["name"], resp)
        return resp

    def get(self, id=None, data=None, errors=None, error_summary=None):
        context, id = self._prepare(id)
        data_dict = {"id": id}
        try:
            old_data = logic.get_action("user_show")(context, data_dict)

            g.display_name = old_data.get("display_name")
            g.user_name = old_data.get("name")

            data = data or old_data

        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit user %s") % "")
        except logic.NotFound:
            base.abort(404, _("User not found"))
        user_obj = context.get("user_obj")

        errors = errors or {}
        vars = {"data": data, "errors": errors, "error_summary": error_summary}

        extra_vars = _extra_template_variables(
            {"model": model, "session": model.Session, "user": g.user}, data_dict
        )

        extra_vars["show_email_notifications"] = asbool(
            config.get("ckan.activity_streams_email_notifications")
        )
        vars.update(extra_vars)
        extra_vars["form"] = base.render(edit_user_form, extra_vars=vars)

        return base.render("user/edit.html", extra_vars)


# qualhb 29/06
class EditViewUser(MethodView):
    def _prepare(self, id):
        context = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        if id is None:
            if g.userobj:
                id = g.userobj.id
            else:
                base.abort(400, _("No user specified"))
        data_dict = {"id": id}

        try:
            logic.check_access("user_update", context, data_dict)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit a user."))
        return context, id

    def post(self, id=None):
        context, id = self._prepare(id)
        if not context["save"]:
            return self.get(id)

        if id in (g.userobj.id, g.userobj.name):
            current_user = True
        else:
            current_user = False
        old_username = g.userobj.name

        try:
            data_dict = logic.clean_dict(
                dictization_functions.unflatten(
                    logic.tuplize_dict(logic.parse_params(request.form))
                )
            )
            data_dict.update(
                logic.clean_dict(
                    dictization_functions.unflatten(
                        logic.tuplize_dict(logic.parse_params(request.files))
                    )
                )
            )

        except dictization_functions.DataError:
            base.abort(400, _("Integrity Error"))
        data_dict.setdefault("activity_streams_email_notifications", False)

        context["message"] = data_dict.get("log_message", "")
        data_dict["id"] = id
        data_dict["about"] = (
            str(data_dict["phone"]),
            str(data_dict["passport"]),
            str(data_dict["abouts"]),
        )

        data_dict_old = {"id": id}
        old_data = logic.get_action("user_show")(context, data_dict_old)

        old_data["id"] = id
        old_data["fullname"] = data_dict["fullname"]
        old_data["about"] = (
            str(data_dict["phone"]),
            str(data_dict["passport"]),
            str(data_dict["abouts"]),
            None,
            str(data_dict["id_organ"]),
            str(data_dict["role"]),
        )

        id_organ = data_dict["id_organ"]
        username = data_dict["name"]
        role = data_dict["role"]
        old_data_delete = logic.get_action("user_show")(context, data_dict_old)
        about_old = old_data_delete["about"]
        id_organ_delete = h.get_phone_pass(about_old)
        id_organ_old = id_organ_delete[4]

        context_organ = {"model": model, "session": model.Session, "user": g.user}

        data_dict_organ_delete = {"id": id_organ_old, "username": username}

        data_dict_organ = {"id": id_organ, "username": username, "role": role}

        logic.get_action("group_member_delete")(context_organ, data_dict_organ_delete)
        logic.get_action("group_member_create")(context_organ, data_dict_organ)

        try:
            user = logic.get_action("user_update")(context, old_data)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit user %s") % id)
        except logic.NotFound:
            base.abort(404, _("User not found"))
        except logic.ValidationError as e:
            errors = e.error_dict
            error_summary = e.error_summary
            return self.get(id, data_dict, errors, error_summary)

        h.flash_success(_("Profile updated"))
        resp = h.redirect_to("user.index")
        if current_user and data_dict["name"] != old_username:
            # Changing currently logged in user's name.
            # Update repoze.who cookie to match
            set_repoze_user(data_dict["name"], resp)
        return resp

    def get(self, id=None, data=None, errors=None, error_summary=None):
        context, id = self._prepare(id)
        data_dict = {"id": id}
        try:
            old_data = logic.get_action("user_show")(context, data_dict)

            g.display_name = old_data.get("display_name")
            g.user_name = old_data.get("name")

            data = data or old_data

        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit user %s") % "")
        except logic.NotFound:
            base.abort(404, _("User not found"))
        user_obj = context.get("user_obj")

        org_lists = get_organs_list()
        organs = []
        for org in org_lists:
            organs.append(dict(text=org["display_name"], value=org["id"]))

        context = {"model": model, "session": model.Session, "user": g.user}

        roles = logic.get_action("member_roles_list")(
            context, {"group_type": "organization"}
        )

        errors = errors or {}
        vars = {
            "data": data,
            "errors": errors,
            "error_summary": error_summary,
            "organs": organs,
            "roles": roles,
        }

        extra_vars = _extra_template_variables(
            {"model": model, "session": model.Session, "user": g.user}, data_dict
        )

        extra_vars["show_email_notifications"] = asbool(
            config.get("ckan.activity_streams_email_notifications")
        )
        vars.update(extra_vars)
        extra_vars["form"] = base.render(edit_user_form_acc, extra_vars=vars)

        return base.render("user/edit_user.html", extra_vars)


class EditViewContactUser(MethodView):
    def _prepare(self, id):
        context = {
            "save": "save" in request.form,
            "schema": _edit_form_to_db_schema(),
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        if id is None:
            if g.userobj:
                id = g.userobj.id
            else:
                base.abort(400, _("No user specified"))
        data_dict = {"id": id}

        try:
            logic.check_access("user_update", context, data_dict)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit a user."))
        return context, id

    def post(self, id=None):
        context, id = self._prepare(id)
        if not context["save"]:
            return self.get(id)

        if id in (g.userobj.id, g.userobj.name):
            current_user = True
        else:
            current_user = False
        old_username = g.userobj.name

        try:
            data_dict = logic.clean_dict(
                dictization_functions.unflatten(
                    logic.tuplize_dict(logic.parse_params(request.form))
                )
            )
            data_dict.update(
                logic.clean_dict(
                    dictization_functions.unflatten(
                        logic.tuplize_dict(logic.parse_params(request.files))
                    )
                )
            )

        except dictization_functions.DataError:
            base.abort(400, _("Integrity Error"))
        data_dict.setdefault("activity_streams_email_notifications", False)

        context["message"] = data_dict.get("log_message", "")
        data_dict["id"] = id
        data_dict["about"] = (
            str(data_dict["phone"]),
            str(data_dict["passport"]),
            str(data_dict["abouts"]),
        )

        data_dict_old = {"id": id}
        old_data = logic.get_action("user_show")(context, data_dict_old)

        old_data["id"] = id
        old_data["fullname"] = data_dict["fullname"]
        old_data["about"] = (
            str(data_dict["phone"]),
            str(data_dict["passport"]),
            str(data_dict["abouts"]),
            None,
            str(data_dict["address"]),
            str(data_dict["date"]),
            str(data_dict["detail"]),
            str(data_dict["city"]),
            str(data_dict["district"]),
            str(data_dict["ward"]),
            str(data_dict["id_organ"]),
            str(data_dict["organ_under"]),
        )

        id_organ = data_dict["id_organ"]
        username = data_dict["name"]
        role = data_dict["role"]
        old_data_delete = logic.get_action("user_show")(context, data_dict_old)
        about_old = old_data_delete["about"]
        id_organ_delete = h.get_phone_pass(about_old)
        id_organ_old = id_organ_delete[4]

        context_organ = {"model": model, "session": model.Session, "user": g.user}

        data_dict_organ_delete = {"id": id_organ_old, "username": username}

        data_dict_organ = {"id": id_organ, "username": username, "role": role}

        logic.get_action("group_member_delete")(context_organ, data_dict_organ_delete)
        logic.get_action("group_member_create")(context_organ, data_dict_organ)

        try:
            user = logic.get_action("user_update")(context, old_data)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit user %s") % id)
        except logic.NotFound:
            base.abort(404, _("User not found"))
        except logic.ValidationError as e:
            errors = e.error_dict
            error_summary = e.error_summary
            return self.get(id, data_dict, errors, error_summary)

        h.flash_success(_("Profile updated"))
        resp = h.redirect_to("user.index")
        if current_user and data_dict["name"] != old_username:
            # Changing currently logged in user's name.
            # Update repoze.who cookie to match
            set_repoze_user(data_dict["name"], resp)
        return resp

    def get(self, id=None, data=None, errors=None, error_summary=None):
        context, id = self._prepare(id)
        data_dict = {"id": id}
        try:
            old_data = logic.get_action("user_show")(context, data_dict)

            g.display_name = old_data.get("display_name")
            g.user_name = old_data.get("name")

            data = data or old_data

        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to edit user %s") % "")
        except logic.NotFound:
            base.abort(404, _("User not found"))
        user_obj = context.get("user_obj")

        context = {"model": model, "session": model.Session, "user": g.user}

        errors = errors or {}
        vars = {"data": data, "errors": errors, "error_summary": error_summary}

        extra_vars = _extra_template_variables(
            {"model": model, "session": model.Session, "user": g.user}, data_dict
        )

        extra_vars["show_email_notifications"] = asbool(
            config.get("ckan.activity_streams_email_notifications")
        )
        vars.update(extra_vars)
        extra_vars["form"] = base.render(edit_user_form_contact, extra_vars=vars)

        return base.render("user/edit_contact_user.html", extra_vars)


class RegisterView(MethodView):
    def _prepare(self):
        context = {
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
            "schema": _new_form_to_db_schema(),
            "save": "save" in request.form,
        }
        try:
            logic.check_access("user_create", context)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to register as a user."))
        return context

    def post(self):
        context = self._prepare()
        try:
            data_dict = logic.clean_dict(
                dictization_functions.unflatten(
                    logic.tuplize_dict(logic.parse_params(request.form))
                )
            )
            data_dict.update(
                logic.clean_dict(
                    dictization_functions.unflatten(
                        logic.tuplize_dict(logic.parse_params(request.files))
                    )
                )
            )

        except dictization_functions.DataError:
            base.abort(400, _("Integrity Error"))

        context["message"] = data_dict.get("log_message", "")
        try:
            captcha.check_recaptcha(request)
        except captcha.CaptchaError:
            error_msg = _("Bad Captcha. Please try again.")
            h.flash_error(error_msg)
            return self.get(data_dict)

        data_dict["about"] = (
            str(data_dict["phone"]),
            str(data_dict["passport"]),
            None,
            str(data_dict["desc"]),
            str(data_dict["address"]),
            str(data_dict["date"]),
            str(data_dict["detail"]),
            str(data_dict["city"]),
            str(data_dict["district"]),
            str(data_dict["ward"]),
            str(data_dict["id_organ"]),
            str(data_dict["organ_under"]),
        )

        try:
            logic.get_action("user_create")(context, data_dict)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to create user %s") % "")
        except logic.NotFound:
            base.abort(404, _("User not found"))
        except logic.ValidationError as e:
            errors = e.error_dict
            error_summary = e.error_summary
            return self.get(data_dict, errors, error_summary)
        h.flash_success("Đăng ký tài khoản thành công, vui lòng chờ duyệt")
        if g.user:
            # #1799 User has managed to register whilst logged in - warn user
            # they are not re-logged in as new user.
            h.flash_success(
                _(
                    'Người dùng "%s" hiện đã được đăng ký nhưng bạn '
                    'đang đăng nhập dưới tài khoản "%s" từ trước'
                )
                % (data_dict["name"], g.user)
            )
            if authz.is_sysadmin(g.user):
                # the sysadmin created a new user. We redirect him to the
                # activity page for the newly created user
                return h.redirect_to("user.activity", id=data_dict["name"])
            else:
                return base.render("user/logout_first.html")

        # log the user in programatically
        resp = h.redirect_to("user.me")
        set_repoze_user(data_dict["name"], resp)
        return resp

    def get(self, data=None, errors=None, error_summary=None):
        self._prepare()

        if g.user and not data and not authz.is_sysadmin(g.user):
            # #1799 Don't offer the registration form if already logged in
            return base.render("user/logout_first.html", {})

        org_lists = get_organs_list()
        organs = []
        organs.append(dict(text="", value=""))
        for org in org_lists:
            organs.append(dict(text=org["display_name"], value=org["id"]))

        form_vars = {
            "data": data or {},
            "errors": errors or {},
            "error_summary": error_summary or {},
            "organs": organs,
        }

        extra_vars = {
            "is_sysadmin": authz.is_sysadmin(g.user),
            "form": base.render(new_user_form, form_vars),
        }
        return base.render("user/new.html", extra_vars)


# quanlhb 21/02
class RegisterViewOrgan(MethodView):
    def _prepare(self):
        context = {
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
            "schema": _new_form_to_db_schema(),
            "save": "save" in request.form,
        }
        try:
            logic.check_access("user_create", context)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to register as a user."))
        return context

    def post(self):
        context = self._prepare()
        try:
            data_dict = logic.clean_dict(
                dictization_functions.unflatten(
                    logic.tuplize_dict(logic.parse_params(request.form))
                )
            )
            data_dict.update(
                logic.clean_dict(
                    dictization_functions.unflatten(
                        logic.tuplize_dict(logic.parse_params(request.files))
                    )
                )
            )

        except dictization_functions.DataError:
            base.abort(400, _("Integrity Error"))

        context["message"] = data_dict.get("log_message", "")
        try:
            captcha.check_recaptcha(request)
        except captcha.CaptchaError:
            error_msg = _("Bad Captcha. Please try again.")
            h.flash_error(error_msg)
            return self.get(data_dict)

        try:
            logic.get_action("user_create")(context, data_dict)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to create user %s") % "")
        except logic.NotFound:
            base.abort(404, _("User not found"))
        except logic.ValidationError as e:
            errors = e.error_dict
            error_summary = e.error_summary
            return self.get(data_dict, errors, error_summary)

        if g.user:
            # #1799 User has managed to register whilst logged in - warn user
            # they are not re-logged in as new user.
            h.flash_success(
                _(
                    'Người dùng "%s" hiện đã được đăng ký nhưng bạn '
                    'đang đăng nhập dưới tài khoản "%s" từ trước'
                )
                % (data_dict["name"], g.user)
            )
            if authz.is_sysadmin(g.user):
                # the sysadmin created a new user. We redirect him to the
                # activity page for the newly created user
                return h.redirect_to("user.activity", id=data_dict["name"])
            else:
                return base.render("user/logout_first.html")

        # log the user in programatically
        resp = h.redirect_to("user.me")
        set_repoze_user(data_dict["name"], resp)
        return resp

    def get(self, data=None, errors=None, error_summary=None):
        self._prepare()

        if g.user and not data and not authz.is_sysadmin(g.user):
            # #1799 Don't offer the registration form if already logged in
            return base.render("user/logout_first.html", {})

        form_vars = {
            "data": data or {},
            "errors": errors or {},
            "error_summary": error_summary or {},
        }

        extra_vars = {
            "is_sysadmin": authz.is_sysadmin(g.user),
            "form": base.render(new_user_form_organ, form_vars),
        }
        return base.render("user/new.html", extra_vars)


# quanlhb 17/04
class RegisterViewAdd(MethodView):
    def _prepare(self):
        context = {
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
            "schema": _new_form_to_db_schema(),
            "save": "save" in request.form,
        }
        try:
            logic.check_access("user_create", context)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to register as a user."))
        return context

    def post(self):
        q = request.params.get("q", "")
        order_by = request.params.get("order_by", "name")
        context_list = {
            "return_query": True,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        data_dict_list = {"q": q, "order_by": order_by}
        users = []
        users_list = logic.get_action("user_list_delete")(context_list, data_dict_list)
        page = h.Page(collection=users_list, item_count=users_list.count())
        total_user = page.items
        for user in total_user:
            users.append(user[0].name)

        context = self._prepare()

        try:
            data_dict = logic.clean_dict(
                dictization_functions.unflatten(
                    logic.tuplize_dict(logic.parse_params(request.form))
                )
            )
            data_dict.update(
                logic.clean_dict(
                    dictization_functions.unflatten(
                        logic.tuplize_dict(logic.parse_params(request.files))
                    )
                )
            )
        except dictization_functions.DataError:
            base.abort(400, _("Integrity Error"))

        ad = g.userobj.id
        id_organ = data_dict["id_organ"]
        username = data_dict["name"]
        role = data_dict["role"]
        data_dict["about"] = (
            str(data_dict["phone"]),
            str(data_dict["passport"]),
            None,
            None,
            str(data_dict["id_organ"]),
            str(data_dict["role"]),
        )

        context_organ = {"model": model, "session": model.Session, "user": g.user}

        data_dict_organ = {"id": id_organ, "username": username, "role": role}

        context_pending = {
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }

        data_dict_pending = {"id": ad, "include_plugin_extras": True}
        all_role = logic.get_action("get_roles_pending")(
            context_pending, data_dict_pending
        )
        data_dict_patch = dict(all_role)

        context["message"] = data_dict.get("log_message", "")
        try:
            captcha.check_recaptcha(request)
        except captcha.CaptchaError:
            error_msg = _("Bad Captcha. Please try again.")
            h.flash_error(error_msg)
            return self.get(data_dict)

        if username in users:
            data_dict_old = {"id": username}
            old_data = logic.get_action("user_show")(context, data_dict_old)

            old_data["fullname"] = data_dict["fullname"]
            old_data["email"] = data_dict["email"]
            old_data["password1"] = "123abc@A"
            old_data["password2"] = "123abc@A"
            old_data["about"] = (
                str(data_dict["phone"]),
                str(data_dict["passport"]),
                None,
                None,
                str(data_dict["id_organ"]),
                str(data_dict["role"]),
            )
            old_data["plugin_extras"] = data_dict_patch
            old_data["state"] = model.State.ACTIVE

            logic.get_action("user_update")(context, old_data)
            h.flash_success("Thêm mới người dùng %s thành công" % username)
        else:
            try:
                logic.get_action("user_create")(context, data_dict)
            except logic.NotAuthorized:
                base.abort(403, _("Unauthorized to create user %s") % "")
            except logic.NotFound:
                base.abort(404, _("User not found"))
            except logic.ValidationError as e:
                errors = e.error_dict
                error_summary = e.error_summary
                return self.get(data_dict, errors, error_summary)
            # quanlhb 28/06
            if g.user:
                # #1799 User has managed to register whilst logged in - warn user
                # they are not re-logged in as new user.
                h.flash_success('Thêm mới người dùng "%s" thành công' % username)

                try:
                    logic.get_action("user_patch")(
                        context_pending, {"id": username, "state": model.State.ACTIVE}
                    )
                except logic.NotAuthorized:
                    h.flash_error(_("Unauthorized to edit user %s") % username)

                logic.get_action("user_patch")(
                    context_pending, {"id": username, "plugin_extras": data_dict_patch}
                )

                try:
                    logic.get_action("group_member_create")(
                        context_organ, data_dict_organ
                    )
                except logic.NotAuthorized:
                    base.abort(403, _("Unauthorized to add member to group %s") % "")
                except logic.NotFound:
                    base.abort(404, _("Group not found"))
                except logic.ValidationError as e:
                    for _, error in e.error_summary.items():
                        h.flash_error(error)

                if authz.is_sysadmin(g.user):
                    # the sysadmin created a new user. We redirect him to the
                    # activity page for the newly created user
                    return h.redirect_to("user.activity_user", id=data_dict["name"])
                else:
                    return base.render("user/logout_first.html")

        # log the user in programatically
        resp = h.redirect_to("user.index")
        return resp

    def get(self, data=None, errors=None, error_summary=None):
        self._prepare()

        if g.user and not data and not authz.is_sysadmin(g.user):
            # #1799 Don't offer the registration form if already logged in
            return base.render("user/logout_first.html", {})

        org_lists = get_organs_list()
        organs = []
        for org in org_lists:
            organs.append(dict(text=org["display_name"], value=org["id"]))

        context = {"model": model, "session": model.Session, "user": g.user}

        roles = logic.get_action("member_roles_list")(
            context, {"group_type": "organization"}
        )

        form_vars = {
            "data": data or {},
            "errors": errors or {},
            "error_summary": error_summary or {},
            "organs": organs,
            "roles": roles,
        }

        extra_vars = {
            "is_sysadmin": authz.is_sysadmin(g.user),
            "form": base.render(new_user_form_add, form_vars),
        }
        return base.render("user/new.html", extra_vars)


def login():
    # Do any plugin login stuff
    for item in plugins.PluginImplementations(plugins.IAuthenticator):
        response = item.login()
        if response:
            return response

    extra_vars = {}
    if g.user:
        return base.render("user/logout_first.html", extra_vars)

    came_from = request.params.get("came_from")
    if not came_from:
        came_from = h.url_for("user.logged_in")
    g.login_handler = h.url_for(
        _get_repoze_handler("login_handler_path"), came_from=came_from
    )
    return base.render("user/login.html", extra_vars)


def logged_in():
    # redirect if needed
    came_from = request.params.get("came_from", "")
    if h.url_is_local(came_from):
        return h.redirect_to(str(came_from))

    if g.user:
        return me()
    else:
        err = _("Login failed. Bad username or password.")
        h.flash_error(err)
        return login()


def logout():
    # Do any plugin logout stuff
    for item in plugins.PluginImplementations(plugins.IAuthenticator):
        response = item.logout()
        if response:
            return response

    url = h.url_for("user.logged_out_page")
    return h.redirect_to(
        _get_repoze_handler("logout_handler_path") + "?came_from=" + url, parse_url=True
    )


def logged_out():
    # redirect if needed
    came_from = request.params.get("came_from", "")
    if h.url_is_local(came_from):
        return h.redirect_to(str(came_from))
    return h.redirect_to("user.logged_out_page")


def logged_out_page():
    return base.render("user/logout.html", {})


def delete(id):
    """Delete user with id passed as parameter"""
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }
    data_dict = {"id": id}

    try:
        logic.get_action("user_delete")(context, data_dict)
    except logic.NotAuthorized:
        msg = _('Unauthorized to delete user with id "{user_id}".')
        base.abort(403, msg.format(user_id=id))

    if g.userobj.id == id:
        return logout()
    else:
        user_index = h.url_for("user.index")
        return h.redirect_to(user_index)


# quanlhb 27/05
def delete_pending(id):
    """Delete user with id passed as parameter"""
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }
    data_dict = {"id": id}

    try:
        logic.get_action("user_delete_pending")(context, data_dict)
    except logic.NotAuthorized:
        msg = _('Unauthorized to delete user with id "{user_id}".')
        base.abort(403, msg.format(user_id=id))

    if g.userobj.id == id:
        return logout()
    else:
        user_index_pending = h.url_for("user.index_pending")
        return h.redirect_to(user_index_pending)


def generate_apikey(id=None):
    """Cycle the API key of a user"""
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }
    if id is None:
        if g.userobj:
            id = g.userobj.id
        else:
            base.abort(400, _("No user specified"))
    data_dict = {"id": id}

    try:
        result = logic.get_action("user_generate_apikey")(context, data_dict)
    except logic.NotAuthorized:
        base.abort(403, _("Unauthorized to edit user %s") % "")
    except logic.NotFound:
        base.abort(404, _("User not found"))

    h.flash_success(_("Profile updated"))
    return h.redirect_to("user.read", id=result["name"])


def activity(id, offset=0):
    """Render this user's public activity stream page."""

    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
        "for_view": True,
    }
    data_dict = {"id": id, "user_obj": g.userobj, "include_num_followers": True}
    try:
        logic.check_access("user_show", context, data_dict)
    except logic.NotAuthorized:
        base.abort(403, _("Not authorized to see this page"))

    current_user = context["user"]
    current_user1 = str(data_dict["id"])
    if authz.is_sysadmin(g.user):
        pass
    elif current_user != current_user1:
        return base.abort(403, _("Not authorized to see this page"))

    extra_vars = _extra_template_variables(context, data_dict)

    try:
        extra_vars["user_activity_stream"] = logic.get_action("user_activity_list")(
            context, {"id": extra_vars["user_dict"]["id"], "offset": offset}
        )
    except logic.ValidationError:
        base.abort(400)
    extra_vars["id"] = id

    return base.render("user/activity_stream.html", extra_vars)


# quanlhb 04/07
def activity_user(id, offset=0):
    """Render this user's public activity stream page."""

    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
        "for_view": True,
    }
    data_dict = {"id": id, "user_obj": g.userobj, "include_num_followers": True}
    try:
        logic.check_access("user_show", context, data_dict)
    except logic.NotAuthorized:
        base.abort(403, _("Not authorized to see this page"))

    current_user = context["user"]
    current_user1 = str(data_dict["id"])
    if authz.is_sysadmin(g.user):
        pass
    elif current_user != current_user1:
        return base.abort(403, _("Not authorized to see this page"))

    extra_vars = _extra_template_variables(context, data_dict)

    try:
        extra_vars["user_activity_stream"] = logic.get_action("user_activity_list")(
            context, {"id": extra_vars["user_dict"]["id"], "offset": offset}
        )
    except logic.ValidationError:
        base.abort(400)
    extra_vars["id"] = id

    return base.render("user/activity_stream_user.html", extra_vars)


class RequestResetView(MethodView):
    def _prepare(self):
        context = {
            "model": model,
            "session": model.Session,
            "user": g.user,
            "auth_user_obj": g.userobj,
        }
        try:
            logic.check_access("request_reset", context)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to request reset password."))

    def post(self):
        self._prepare()
        id = request.form.get("user")
        if id in (None, ""):
            h.flash_error(_("Email is required"))
            return h.redirect_to("/user/reset")
        log.info('Password reset requested for user "{}"'.format(id))

        context = {"model": model, "user": g.user, "ignore_auth": True}
        user_objs = []

        # Usernames cannot contain '@' symbols
        if "@" in id:
            # Search by email address
            # (You can forget a user id, but you don't tend to forget your
            # email)
            user_list = logic.get_action("user_list")(context, {"email": id})
            if user_list:
                # send reset emails for *all* user accounts with this email
                # (otherwise we'd have to silently fail - we can't tell the
                # user, as that would reveal the existence of accounts with
                # this email address)
                for user_dict in user_list:
                    # This is ugly, but we need the user object for the mailer,
                    # and user_list does not return them
                    logic.get_action("user_show")(context, {"id": user_dict["id"]})
                    user_objs.append(context["user_obj"])

        else:
            # Search by user name
            # (this is helpful as an option for a user who has multiple
            # accounts with the same email address and they want to be
            # specific)
            try:
                logic.get_action("user_show")(context, {"id": id})
                user_objs.append(context["user_obj"])
            except logic.NotFound:
                pass

        if not user_objs:
            log.info("User requested reset link for unknown user: {}".format(id))

        for user_obj in user_objs:
            log.info("Emailing reset link to user: {}".format(user_obj.name))
            try:
                # FIXME: How about passing user.id instead? Mailer already
                # uses model and it allow to simplify code above
                mailer.send_reset_link(user_obj)
            except mailer.MailerException as e:
                # SMTP is not configured correctly or the server is
                # temporarily unavailable
                h.flash_error(
                    _(
                        "Error sending the email. Try again later "
                        "or contact an administrator for help"
                    )
                )
                log.exception(e)
                return h.redirect_to("home.index")

        # always tell the user it succeeded, because otherwise we reveal
        # which accounts exist or not
        h.flash_success(
            _(
                "A reset link has been emailed to you "
                "(unless the account specified does not exist)"
            )
        )
        return h.redirect_to("home.index")

    def get(self):
        self._prepare()
        return base.render("user/request_reset.html", {})


class PerformResetView(MethodView):
    def _prepare(self, id):
        # FIXME 403 error for invalid key is a non helpful page
        context = {
            "model": model,
            "session": model.Session,
            "user": id,
            "keep_email": True,
        }

        try:
            logic.check_access("user_reset", context)
        except logic.NotAuthorized:
            base.abort(403, _("Unauthorized to reset password."))

        try:
            user_dict = logic.get_action("user_show")(context, {"id": id})
        except logic.NotFound:
            base.abort(404, _("User not found"))
        user_obj = context["user_obj"]
        g.reset_key = request.params.get("key")
        if not mailer.verify_reset_link(user_obj, g.reset_key):
            msg = _("Invalid reset key. Please try again.")
            h.flash_error(msg)
            base.abort(403, msg)
        return context, user_dict

    def _get_form_password(self):
        password1 = request.form.get("password1")
        password2 = request.form.get("password2")
        if password1 is not None and password1 != "":
            if len(password1) < 8:
                raise ValueError(_("Your password must be 8 " "characters or longer."))
            elif password1 != password2:
                raise ValueError(_("The passwords you entered" " do not match."))
            return password1
        msg = _("You must provide a password")
        raise ValueError(msg)

    def post(self, id):
        context, user_dict = self._prepare(id)
        context["reset_password"] = True
        user_state = user_dict["state"]
        try:
            new_password = self._get_form_password()
            user_dict["password"] = new_password
            username = request.form.get("name")
            if username is not None and username != "":
                user_dict["name"] = username
            user_dict["reset_key"] = g.reset_key
            updated_user = logic.get_action("user_update")(context, user_dict)
            # Users can not change their own state, so we need another edit
            if updated_user["state"] == model.State.PENDING:
                patch_context = {
                    "user": logic.get_action("get_site_user")(
                        {"ignore_auth": True}, {}
                    )["name"]
                }
                logic.get_action("user_patch")(
                    patch_context, {"id": user_dict["id"], "state": model.State.ACTIVE}
                )
            mailer.create_reset_key(context["user_obj"])
            h.flash_success(_("Your password has been reset."))
            return h.redirect_to("home.index")
        except logic.NotAuthorized:
            h.flash_error(_("Unauthorized to edit user %s") % id)
        except logic.NotFound:
            h.flash_error(_("User not found"))
        except dictization_functions.DataError:
            h.flash_error(_("Integrity Error"))
        except logic.ValidationError as e:
            h.flash_error("%r" % e.error_dict)
        except ValueError as e:
            h.flash_error(text_type(e))
        user_dict["state"] = user_state
        return base.render("user/perform_reset.html", {"user_dict": user_dict})

    def get(self, id):
        context, user_dict = self._prepare(id)
        return base.render("user/perform_reset.html", {"user_dict": user_dict})


def follow(id):
    """Start following this user."""
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }
    data_dict = {"id": id, "include_num_followers": True}
    try:
        logic.get_action("follow_user")(context, data_dict)
        user_dict = logic.get_action("user_show")(context, data_dict)
        h.flash_success(
            _("You are now following {0}").format(user_dict["display_name"])
        )
    except logic.ValidationError as e:
        error_message = e.message or e.error_summary or e.error_dict
        h.flash_error(error_message)
    except (logic.NotFound, logic.NotAuthorized) as e:
        h.flash_error(e.message)
    return h.redirect_to("user.read", id=id)


def unfollow(id):
    """Stop following this user."""
    context = {
        "model": model,
        "session": model.Session,
        "user": g.user,
        "auth_user_obj": g.userobj,
    }
    data_dict = {"id": id, "include_num_followers": True}
    try:
        logic.get_action("unfollow_user")(context, data_dict)
        user_dict = logic.get_action("user_show")(context, data_dict)
        h.flash_success(
            _("You are no longer following {0}").format(user_dict["display_name"])
        )
    except logic.ValidationError as e:
        error_message = e.error_summary or e.message or e.error_dict
        h.flash_error(error_message)
    except (logic.NotFound, logic.NotAuthorized) as e:
        h.flash_error(e.message)
    return h.redirect_to("user.read", id=id)


def followers(id):
    context = {"for_view": True, "user": g.user, "auth_user_obj": g.userobj}
    data_dict = {"id": id, "user_obj": g.userobj, "include_num_followers": True}
    extra_vars = _extra_template_variables(context, data_dict)
    f = logic.get_action("user_follower_list")
    try:
        extra_vars["followers"] = f(context, {"id": extra_vars["user_dict"]["id"]})
    except logic.NotAuthorized:
        base.abort(403, _("Unauthorized to view followers %s") % "")
    return base.render("user/followers.html", extra_vars)


# quanlhb 26/05
# user.add_url_rule(u'/user_pending', view_func=index_pending, strict_slashes=False)

# user.add_url_rule(u'/', view_func=index, strict_slashes=False)

# giangptd15/06
user.add_url_rule("/index", view_func=Index.as_view(str("index")))
user.add_url_rule("/index_organ", view_func=Index_organ.as_view(str("index_organ")))
user.add_url_rule("/index_pending", view_func=ListPending.as_view(str("index_pending")))
user.add_url_rule("/me", view_func=me)

_edit_view = EditView.as_view(str("edit"))
user.add_url_rule("/edit", view_func=_edit_view)
user.add_url_rule("/edit/<id>", view_func=_edit_view)

# quanlhb 29/06
_edit_view_user = EditViewUser.as_view(str("edit_user"))
user.add_url_rule("/edit_user", view_func=_edit_view_user)
user.add_url_rule("/edit_user/<id>", view_func=_edit_view_user)

_edit_view_contact_user = EditViewContactUser.as_view(str("edit_contact_user"))
user.add_url_rule("/edit_contact_user", view_func=_edit_view_contact_user)
user.add_url_rule("/edit_contact_user/<id>", view_func=_edit_view_contact_user)

user.add_url_rule("/register", view_func=RegisterView.as_view(str("register")))

# quanlhb 21/02
user.add_url_rule(
    "/register_organ", view_func=RegisterViewOrgan.as_view(str("register_organ"))
)

# quanlhb 17/04
user.add_url_rule(
    "/register_add", view_func=RegisterViewAdd.as_view(str("register_add"))
)

user.add_url_rule("/login", view_func=login)
user.add_url_rule("/logged_in", view_func=logged_in)
user.add_url_rule("/_logout", view_func=logout)
user.add_url_rule("/logged_out", view_func=logged_out)
user.add_url_rule("/logged_out_redirect", view_func=logged_out_page)

user.add_url_rule("/delete/<id>", view_func=delete, methods=("POST",))
# quanlhb 27/05
user.add_url_rule("/delete_pending/<id>", view_func=delete_pending, methods=("POST",))
# quanlhb 14/06
user.add_url_rule("/unlock_user/<id>", view_func=unlock_user, methods=("POST",))

# giangptd 12/06
user.add_url_rule("/accept_pending/<id>", view_func=accept_pending, methods=("POST",))
user.add_url_rule("/lock_user/<id>", view_func=lock_user, methods=("POST",))
user.add_url_rule(
    "/delete_request_user/<id>", view_func=delete_request_user, methods=("POST",)
)
user.add_url_rule("/reset_password/<id>", view_func=reset_password, methods=("POST",))
user.add_url_rule("/permissions", view_func=Permissions.as_view(str("permissions")))
user.add_url_rule(
    "/delete_permission/<id>", view_func=delete_permission, methods=("POST",)
)
user.add_url_rule(
    "/edit_permission/<id>", view_func=EditPermission.as_view(str("edit_permission"))
)

user.add_url_rule("/generate_key", view_func=generate_apikey, methods=("POST",))
user.add_url_rule("/generate_key/<id>", view_func=generate_apikey, methods=("POST",))

user.add_url_rule("/activity/<id>", view_func=activity)
user.add_url_rule("/activity/<id>/<int:offset>", view_func=activity)

# quanlhb 04/07
user.add_url_rule("/activity_user/<id>", view_func=activity_user)
user.add_url_rule("/activity_user/<id>/<int:offset>", view_func=activity_user)

user.add_url_rule("/reset", view_func=RequestResetView.as_view(str("request_reset")))
user.add_url_rule(
    "/reset/<id>", view_func=PerformResetView.as_view(str("perform_reset"))
)

user.add_url_rule("/follow/<id>", view_func=follow, methods=("POST",))
user.add_url_rule("/unfollow/<id>", view_func=unfollow, methods=("POST",))
user.add_url_rule("/followers/<id>", view_func=followers)

# quanlhb 04/07
user.add_url_rule("read_user/<id>", view_func=read_user)

user.add_url_rule("/<id>", view_func=read)
user.add_url_rule("/<id>/api-tokens", view_func=ApiTokenView.as_view(str("api_tokens")))
user.add_url_rule(
    "/<id>/api-tokens/<jti>/revoke", view_func=api_token_revoke, methods=("POST",)
)
