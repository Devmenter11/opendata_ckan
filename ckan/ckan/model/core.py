# encoding: utf-8

log = __import__("logging").getLogger(__name__)


class State(object):
    ACTIVE = "active"
    DELETED = "deleted"
    PENDING = "pending"
    # quanlhb 14/06
    LOCKED = "locked"


class StatefulObjectMixin(object):
    __stateful__ = True

    def delete(self):
        log.debug("Running delete on %s", self)
        self.state = State.DELETED

    def undelete(self):
        self.state = State.ACTIVE

    # quanlhb 14/06
    def locked(self):
        self.state = State.LOCKED

    def is_active(self):
        # also support None in case this object is not yet refreshed ...
        return self.state is None or self.state == State.ACTIVE
