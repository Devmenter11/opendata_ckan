
var e = window.matchMedia("(min-width: 769px)");
mediaQuery(e);
e.addListener(mediaQuery);

function mediaQuery(e) {
   if (e.matches) {
      if (document.getElementById('page_pri')) {
         window.addEventListener('load', function() {
            var secondaryHeight = document.getElementById("page_second").clientHeight;
            var primaryHeight = document.getElementById("page_pri").clientHeight;
            if (secondaryHeight >= primaryHeight) {
               document.getElementById("page_pri").style.height = secondaryHeight + "px";
            } else {
               document.getElementById("page_second").style.height = primaryHeight + "px";
            }
         });
      }

      if (document.getElementById('infor_form')) {
         window.addEventListener('DOMContentLoaded', function() {
            var primaryHeight = document.getElementById("infor_form").clientHeight;
            document.getElementById("sidebar_admin").style.height = primaryHeight + "px";
         });
      }
      
      if (document.getElementById('admin-config-form')) {
         window.addEventListener('DOMContentLoaded', function() {
            var primaryHeight = document.getElementById("admin-config-form").clientHeight;
            document.getElementById("sidebar_admin").style.height = primaryHeight + "px";
         });
      }

      if (document.getElementById('bg_index')) {
         window.addEventListener('DOMContentLoaded', function() {
            var primaryHeight = document.getElementById("bg_index").clientHeight;
            document.getElementById("sidebar_admin").style.height = primaryHeight + "px";
         });
      }
   };
};

if (document.getElementById('page_second')) {
   window.addEventListener("orientationchange", function() {
      location.reload();
   });
}