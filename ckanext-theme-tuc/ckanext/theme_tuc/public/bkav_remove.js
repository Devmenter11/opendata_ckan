
// remove img

function removeImg() {
    document.getElementById('field-image-upload').value = '';
    document.getElementById('field-image-url').value = '';
    document.getElementById('btn_del-url').click();
    document.getElementById('remove_img').style.display = 'none';
    document.getElementById('input_upload').style.display = 'block';
};

if (document.getElementById('field-image-upload')) {
    document.getElementById('field-image-upload').addEventListener('change', function() {
        if (document.getElementById('field-image-upload').value != '') {
            document.getElementById('remove_img').style.display = 'block'
        }
    });
}

// show input_url

function btnLink() {
    document.getElementById('input_url').style.display = 'block';
    document.getElementById('input_upload').style.display = 'none';
    document.getElementById('remove_img').style.display = 'block'
    document.getElementById('btn_del-url').click();
};

if (document.getElementById('field-image-url')) {
    if (document.getElementById('field-image-url').value != '') {
        document.getElementById('input_url').style.display = 'block';
        document.getElementById('input_upload').style.display = 'none';
        if (document.getElementById('remove_img')) {
            document.getElementById('remove_img').style.display = 'block';
        };
    } else {
        document.getElementById('input_url').style.display = 'none';
        document.getElementById('input_upload').style.display = 'block';
        document.getElementById('remove_img').style.display = 'none';
    }
}