
if (document.getElementById('fake_name')) {
    var fakeName = document.getElementById('fake_name');
    fakeName.focus();
    fakeName.addEventListener('keyup', function() {
        var valueUrl = fakeName.value.toLowerCase();
        valueUrl = valueUrl.replace(/à|á|ã|â|ă|ạ|ả|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
        valueUrl = valueUrl.replace(/è|é|ê|ẹ|ẻ|ẽ|ề|ế|ệ|ể|ễ/g,"e");
        valueUrl = valueUrl.replace(/ì|í|ĩ|ị|ỉ/g,"i");
        valueUrl = valueUrl.replace(/ò|ó|ô|ơ|ọ|ỏ|õ|ồ|ố|ộ|ổ|ỗ|ờ|ớ|ợ|ở|ỡ/g,"o");
        valueUrl = valueUrl.replace(/ù|ú|ũ|ư|ụ|ủ|ừ|ứ|ự|ử|ữ/g,"u");
        valueUrl = valueUrl.replace(/ý|ỳ|ỵ|ỷ|ỹ/g,"y");
        valueUrl = valueUrl.replace(/đ/g,"d");
        valueUrl = valueUrl.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, "");
        valueUrl = valueUrl.replace(/\u02C6|\u0306|\u031B/g, "");
        valueUrl = valueUrl.replace(/ + /g,"");
        valueUrl = valueUrl.replace(/ - /g,"-");
        valueUrl = valueUrl.trim().replaceAll(" ", "-");
        valueUrl = valueUrl.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|`|{|}|\||\\/g,"");
        document.getElementById('field-url').value = valueUrl;
    });
    document.getElementById("field-url").addEventListener("keydown", function(event) {
      event.preventDefault();
    });
};
// disable item
if (document.getElementById('disable_item')) {
    var input1Element = document.getElementById('field-name');
    var input2Element = document.getElementById('field-url');
    var input3Element = document.getElementById('field-description');
    var input4Element = document.getElementById('field-image-url');
    var upFile = document.getElementById('field-image-upload');
    document.getElementById('disable_item').addEventListener('click', function () {
        input1Element.required = true;
        input2Element.required = true;
        input3Element.required = true;
        if (input4Element.value != '') {
            upFile.required = false;
        } else {
            upFile.required = true;
        }
        if (input1Element.value != '' && input2Element.value != '' && input3Element.value != '' && input4Element.value != '') {
            document.getElementById('loader1').style.display = 'block';
        }
    });

    // only photos can be uploaded
    upFile.accept = [".png, .jpg, .jpeg"];
    upFile.addEventListener("change", function () {
        var filePath = upFile.value;
        var files = /(\.jpg|\.jpeg|\.png)$/;
        if (!files.exec(filePath)) {
            upFile.value = '';
            alert('Vui lòng tải lên tệp có định dạng: .jpg/.jpeg/.png');
            document.getElementById('input_upload').children[0].classList.add('up_file-img');
            document.getElementById('input_url').style.display = 'none';
        } else {
            document.getElementById('input_url').style.display = 'block';
            document.getElementById('input_upload').style.display = 'none';
        }
    });
};
